#include <iostream>

int main() {
  std::cout << "You are using... ";

#ifdef __linux__
  std::cout << "LINUX";
#elif __UNIX__
  std::cout << "a UNIX variant";
#elif __APPLE__
  std::cout << "MACOS";
#elif __WIN32__
  std::cout << "WINDOWS";
#endif

  std::cout << "!" << std::endl;
  return 0;
}
